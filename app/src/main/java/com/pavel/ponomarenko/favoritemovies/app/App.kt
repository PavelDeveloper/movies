package com.pavel.ponomarenko.favoritemovies.app

import android.app.Application
import com.pavel.ponomarenko.favoritemovies.di.component.AppComponent
import com.pavel.ponomarenko.favoritemovies.di.component.DaggerAppComponent
import com.pavel.ponomarenko.favoritemovies.di.modules.ApiModule
import com.pavel.ponomarenko.favoritemovies.di.modules.AppModule
import com.pavel.ponomarenko.favoritemovies.di.modules.RepositoryModule


class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
    }

    private fun initializeDagger() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .apiModule(ApiModule())
            .repositoryModule(RepositoryModule())
            .build()
    }
}
