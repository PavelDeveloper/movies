package com.pavel.ponomarenko.favoritemovies.data.detail_model

data class ProductionCountry(
    val iso_3166_1: String,
    val name: String
)