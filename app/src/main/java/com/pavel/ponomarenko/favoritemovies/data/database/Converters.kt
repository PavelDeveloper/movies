package com.pavel.ponomarenko.favoritemovies.data.database

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pavel.ponomarenko.favoritemovies.data.detail_model.Genre
import com.pavel.ponomarenko.favoritemovies.data.detail_model.ProductionCompany
import com.pavel.ponomarenko.favoritemovies.data.detail_model.ProductionCountry
import com.pavel.ponomarenko.favoritemovies.data.detail_model.SpokenLanguage

class Converters {
    companion object {

        @TypeConverter
        @JvmStatic
        fun fromOptionGenreList(genreValues: List<Genre>?): String? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<Genre>>(){}.type
            return gson.toJson(genreValues, listType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionGenreList(optionValuesString: String): List<Genre>? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<Genre>>(){}.type
            return gson.fromJson(optionValuesString, listType)
        }

        @TypeConverter
        @JvmStatic
        fun fromOptionProductionCompanyList(genreValues: List<ProductionCompany>?): String? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<ProductionCompany>>(){}.type
            return gson.toJson(genreValues, listType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionProductionCompanyList(optionValuesString: String): List<ProductionCompany>? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<ProductionCompany>>(){}.type
            return gson.fromJson(optionValuesString, listType)
        }

        @TypeConverter
        @JvmStatic
        fun fromOptionProductionCountryList(genreValues: List<ProductionCountry>?): String? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<ProductionCountry>>(){}.type
            return gson.toJson(genreValues, listType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionProductionCountryList(optionValuesString: String): List<ProductionCountry>? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<ProductionCountry>>(){}.type
            return gson.fromJson(optionValuesString, listType)
        }

        @TypeConverter
        @JvmStatic
        fun fromOptionSpokenLanguageList(genreValues: List<SpokenLanguage>?): String? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<SpokenLanguage>>(){}.type
            return gson.toJson(genreValues, listType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionSpokenLanguageList(optionValuesString: String): List<SpokenLanguage>? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<SpokenLanguage>>(){}.type
            return gson.fromJson(optionValuesString, listType)
        }

    }
}