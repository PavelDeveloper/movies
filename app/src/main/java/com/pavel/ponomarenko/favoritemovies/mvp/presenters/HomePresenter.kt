package com.pavel.ponomarenko.favoritemovies.mvp.presenters

import com.arellomobile.mvp.InjectViewState
import com.pavel.ponomarenko.favoritemovies.app.App
import com.pavel.ponomarenko.favoritemovies.data.favorite_model.Movie
import com.pavel.ponomarenko.favoritemovies.data.repository.Repository
import com.pavel.ponomarenko.favoritemovies.mvp.view.HomeView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@InjectViewState
class HomePresenter : BasePresenter<HomeView>() {

    @Inject
    lateinit var repository: Repository

    private val  moviesIdList: ArrayList<String> = ArrayList()

    init {
        viewState.showProgress()
        App.appComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        GlobalScope.launch(Dispatchers.Main){
            repository.getFavoriteMoviesRepository()?.let {
                viewState.setMovies(it)
                viewState.hideProgress()
                moviesIdList.clear()
                it.forEach { movie ->
                    moviesIdList.add(movie.itemId)
                }
                viewState.getMoviesIdList(moviesIdList)
            }
        }
    }
}