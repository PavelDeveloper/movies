package com.pavel.ponomarenko.favoritemovies.mvp.view

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.pavel.ponomarenko.favoritemovies.data.favorite_model.Movie

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface HomeView : BaseMvpView {
    fun setMovies(data: List<Movie>)
    fun getMoviesIdList(data: List<String>)
}