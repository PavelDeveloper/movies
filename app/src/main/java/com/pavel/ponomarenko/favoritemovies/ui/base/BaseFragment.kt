package com.pavel.ponomarenko.favoritemovies.ui.base

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.pavel.ponomarenko.favoritemovies.mvp.view.BaseMvpView
import com.pavel.ponomarenko.favoritemovies.ui.interfaces.ToolbarActions

abstract class BaseFragment: MvpAppCompatFragment(), BaseMvpView {

    private var toolbar: Toolbar? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(getLayoutRes(), container, false)
        setToolbar()
        setToolbarTitle(getToolbarTitle())
        return view
    }

    protected abstract fun getLayoutRes(): Int
    abstract fun getToolbarTitle(): String

    private fun setToolbar() {
        toolbar = (activity as ToolbarActions).getToolBar()
        toolbar?.let {
            setSupportActionBar(toolbar!!)
        }
    }

    fun getToolbar(): Toolbar? = toolbar

    private fun setSupportActionBar(toolbar: Toolbar) {
        (activity as ToolbarActions).setSupportToolBar(toolbar)
    }

    private fun setToolbarTitle(toolbarTitle: String) {
            (activity as ToolbarActions).setToolbarTitle(toolbarTitle)
    }

}