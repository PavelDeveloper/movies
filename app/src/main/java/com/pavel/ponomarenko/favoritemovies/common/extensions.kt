package com.pavel.ponomarenko.favoritemovies.common

import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.pavel.ponomarenko.favoritemovies.R
import com.pavel.ponomarenko.favoritemovies.R.id


fun AppCompatActivity.replaceFragment(
    fragment: Fragment,
    listener: FragmentManager.OnBackStackChangedListener
) {

    val manager = this.supportFragmentManager
    manager.addOnBackStackChangedListener(listener)
    manager.beginTransaction()
        .replace(
            id.main_container,
            fragment
        ).commit()
}


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun Fragment.replaceFragment(fragment: Fragment, view: View) =
    this.fragmentManager?.let {
        it.beginTransaction()
            .addSharedElement(view, view.transitionName)
            .replace(R.id.main_container, fragment)
            .addToBackStack(null)
            .setReorderingAllowed ( true )
            .commit()
    }
