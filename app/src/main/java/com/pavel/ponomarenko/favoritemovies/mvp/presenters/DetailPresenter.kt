package com.pavel.ponomarenko.favoritemovies.mvp.presenters

import com.arellomobile.mvp.InjectViewState
import com.pavel.ponomarenko.favoritemovies.api.ApiService
import com.pavel.ponomarenko.favoritemovies.app.App
import com.pavel.ponomarenko.favoritemovies.data.repository.Repository
import com.pavel.ponomarenko.favoritemovies.mvp.view.DetailView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@InjectViewState
class DetailPresenter : BasePresenter<DetailView>() {

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var repository: Repository

    init {
        viewState.showProgress()
        App.appComponent.inject(this)
    }

     fun loadInfo(id: String) {
         GlobalScope.launch(Dispatchers.Main){
             repository.getDetailMovieRepository(id).let {
                 viewState.setMovieInfo(it)
             }
             viewState.hideProgress()
         }
    }
}