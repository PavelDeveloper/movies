package com.pavel.ponomarenko.favoritemovies.ui.base

interface ItemModel<in T> {
    fun setData(data: T)
}