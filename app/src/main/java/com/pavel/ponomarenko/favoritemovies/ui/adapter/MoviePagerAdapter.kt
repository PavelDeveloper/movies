package com.pavel.ponomarenko.favoritemovies.ui.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentStatePagerAdapter
import com.pavel.ponomarenko.favoritemovies.ui.fragment.DetailFragment

class MoviePagerAdapter(fragment: Fragment, private val moviesIdList: ArrayList<String>) :
    FragmentStatePagerAdapter(fragment.childFragmentManager) {

    override fun getItem(position: Int): Fragment =
        DetailFragment.newInstance(moviesIdList[position])

    override fun getCount(): Int = moviesIdList.count()
}