package com.pavel.ponomarenko.favoritemovies.ui.interfaces

import android.support.v7.widget.Toolbar

interface ToolbarActions {

    fun getToolBar(): Toolbar

    fun setSupportToolBar(toolbar: Toolbar)

    fun setToolbarTitle(toolbarTitle: String)

    fun showBackButton()

    fun hideBackButton()

}