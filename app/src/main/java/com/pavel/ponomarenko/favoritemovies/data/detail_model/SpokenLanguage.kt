package com.pavel.ponomarenko.favoritemovies.data.detail_model

data class SpokenLanguage(
    val iso_639_1: String,
    val name: String
)