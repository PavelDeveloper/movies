package com.pavel.ponomarenko.favoritemovies.common

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.widget.ImageView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.pavel.ponomarenko.favoritemovies.R
import com.pavel.ponomarenko.favoritemovies.di.modules.GlideApp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


class ImageLoader {

    fun loadImage(context: Context, fragment: Fragment?, imageView: ImageView, url: String) {
        GlideApp.with(context)
            .load(url)
            .listener(
                object: RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        fragment?.startPostponedEnterTransition()
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        fragment?.startPostponedEnterTransition()
                        return false
                    }

                }
            )
            .error(ContextCompat.getDrawable(context, R.drawable.ic_no_photo))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(imageView)
    }

    fun loadImgOld(context: Context, fragment: Fragment?, imageView: ImageView, url: String) {
        GlobalScope.launch(Dispatchers.IO){
            try {
            val connectUrl = URL(url)
            val connection: HttpURLConnection =  connectUrl.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input: InputStream = connection.inputStream
            val myBitmap = BitmapFactory.decodeStream(input)
            launch(Dispatchers.Main) {
                imageView.setImageBitmap(myBitmap)
                fragment?.startPostponedEnterTransition()
            }
            }catch (e: Exception){
                e.printStackTrace()
            }
        }
    }
}