package com.pavel.ponomarenko.favoritemovies.ui.adapter

import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.view.ViewGroup
import com.pavel.ponomarenko.favoritemovies.data.favorite_model.Movie
import com.pavel.ponomarenko.favoritemovies.ui.activity.MainActivity
import com.pavel.ponomarenko.favoritemovies.ui.base.AdapterClickListener
import com.pavel.ponomarenko.favoritemovies.ui.base.BaseAdapter
import com.pavel.ponomarenko.favoritemovies.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.movie_item.view.*
import java.util.concurrent.atomic.AtomicBoolean

class MovieAdapter(private val fragment: Fragment): BaseAdapter<Movie, MovieItemView, BaseViewHolder<MovieItemView>>() {

    private var listener: AdapterClickListener<Movie>? = null


    fun setClickListener(listener: AdapterClickListener<Movie>?) {
        this.listener = listener
    }

    override fun onCreateViewHolder(p0: ViewGroup, position: Int): BaseViewHolder<MovieItemView> {
        val itemView = MovieItemView(p0.context)
        itemView.setClickListener(listener)
        itemView.setFragment(fragment)
        return BaseViewHolder(itemView)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: BaseViewHolder<MovieItemView>, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.view.imageView.transitionName = position.toString() + "_image"
    }
}