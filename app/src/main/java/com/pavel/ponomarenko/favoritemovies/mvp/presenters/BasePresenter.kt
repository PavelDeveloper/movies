package com.pavel.ponomarenko.favoritemovies.mvp.presenters

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView

abstract class BasePresenter<View : MvpView> : MvpPresenter<View>()