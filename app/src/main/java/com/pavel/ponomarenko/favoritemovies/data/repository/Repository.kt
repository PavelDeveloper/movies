package com.pavel.ponomarenko.favoritemovies.data.repository

import com.pavel.ponomarenko.favoritemovies.app.App
import com.pavel.ponomarenko.favoritemovies.common.NetManager
import com.pavel.ponomarenko.favoritemovies.data.detail_model.DetailMovieData
import com.pavel.ponomarenko.favoritemovies.data.favorite_model.Movie
import javax.inject.Inject

class Repository {

    @Inject
    lateinit var netManager: NetManager

    @Inject
    lateinit var localDataSource: RepoLocalDataSource

    @Inject
    lateinit var remoteDataSource: RepoRemoteDataSource

    init {
        App.appComponent.inject(this)
    }

    suspend fun getFavoriteMoviesRepository(): List<Movie>? {
        netManager.isConnectedToInternet?.let {
            if (it) {
                 return remoteDataSource.getFavoriteMovies()
            }
        }
        return localDataSource.getFavoriteMovies()
    }


    suspend fun getDetailMovieRepository(id: String): Movie {
        return localDataSource.getDetailMoviesInfo(id = id)
    }
}