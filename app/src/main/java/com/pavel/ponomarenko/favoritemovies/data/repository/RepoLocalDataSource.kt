package com.pavel.ponomarenko.favoritemovies.data.repository

import com.pavel.ponomarenko.favoritemovies.app.App
import com.pavel.ponomarenko.favoritemovies.data.database.AppDatabase
import com.pavel.ponomarenko.favoritemovies.data.detail_model.DetailMovieData
import com.pavel.ponomarenko.favoritemovies.data.favorite_model.Movie
import javax.inject.Inject

class RepoLocalDataSource {

    @Inject
    lateinit var database: AppDatabase

    init {
        App.appComponent.inject(this)
    }

    fun getFavoriteMovies(): List<Movie>? {
        return database.movieDao().all()
    }

    fun getDetailMoviesInfo(id: String): Movie {
        return database.movieDao().getMovieById(id)
    }
}