package com.pavel.ponomarenko.favoritemovies.di.modules

import android.content.Context
import com.pavel.ponomarenko.favoritemovies.common.NetManager
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

@Module
class NetManagerModule {

    @Singleton
    @Provides
    @Inject
    internal fun provideNetworkManager(context: Context): NetManager {
        return NetManager(context)
    }
}