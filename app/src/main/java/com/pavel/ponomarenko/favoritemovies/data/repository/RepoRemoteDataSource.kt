package com.pavel.ponomarenko.favoritemovies.data.repository

import com.pavel.ponomarenko.favoritemovies.api.ApiService
import com.pavel.ponomarenko.favoritemovies.app.App
import com.pavel.ponomarenko.favoritemovies.data.database.AppDatabase
import com.pavel.ponomarenko.favoritemovies.data.detail_model.DetailMovieData
import com.pavel.ponomarenko.favoritemovies.data.favorite_model.Movie
import com.pavel.ponomarenko.favoritemovies.data.favorite_model.MoviesData
import javax.inject.Inject

class RepoRemoteDataSource {

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var database: AppDatabase

    init {
        App.appComponent.inject(this)

    }

    suspend fun getFavoriteMovies(): List<Movie>  {
        val result = apiService.getMoviesData().await()
        result.body()?.forEach {
            database.movieDao().insertAll(it)
        }
        return  result.body()!!
    }

/*    suspend fun getDetailMoviesInfo(id: String): DetailMovieData? {
        val result = apiService.getDetailMovieData(id).await()
        database.detailDao().insertAll(result.body()!!)
        deleteOldDetailInfo(movieList= database.movieDao().all(),
            detailMovieDataList= database.detailDao().all())
        return  result.body()!!
    }*/

   /* private fun deleteOldDetailInfo (movieList: List<Movie>, detailMovieDataList: List<DetailMovieData>) {

        val movieListId: MutableList<String> = mutableListOf()
        val detailMovieDataListId: MutableList<String> = mutableListOf()

        detailMovieDataList.forEach {
            detailMovieDataListId.add(it.id)
        }

        movieList.forEach {
            movieListId.add(it.id)
        }

        detailMovieDataListId.forEach {
            if (!movieListId.contains(it)) database.detailDao().deleteById(it)
        }
    }*/
}