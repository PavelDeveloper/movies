package com.pavel.ponomarenko.favoritemovies.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.SharedElementCallback
import android.support.v4.view.ViewPager
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pavel.ponomarenko.favoritemovies.R
import com.pavel.ponomarenko.favoritemovies.ui.activity.MainActivity
import com.pavel.ponomarenko.favoritemovies.ui.adapter.MoviePagerAdapter

class PagerFragment : Fragment() {

    private var viewPager: ViewPager? = null
    private val  moviesIdList: ArrayList<String> = ArrayList()

    companion object {
        private const val MOVIE_LIST_KEY_ID = "movie_key_id"
        fun newInstance(moviesIdList: ArrayList<String>): Fragment {
            val fragment = PagerFragment()
            val argument = Bundle()
            argument.putStringArrayList(MOVIE_LIST_KEY_ID, moviesIdList)
            fragment.arguments = argument
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        moviesIdList.addAll(arguments?.getStringArrayList(DetailFragment.MOVIE_KEY_ID)!!)
        viewPager = inflater.inflate(R.layout.fragment_pager, container, false) as ViewPager?
        viewPager?.adapter = MoviePagerAdapter(this, moviesIdList)

        viewPager?.currentItem = MainActivity.currentPosition
        viewPager?.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                MainActivity.currentPosition = position
            }
        })
        prepareSharedElementTransition()

        if (savedInstanceState == null) {
            postponeEnterTransition()
        }
        return viewPager
    }

    private fun prepareSharedElementTransition() {
        val transition =
            TransitionInflater.from(context)
                .inflateTransition(R.transition.change_image_transition)
        sharedElementEnterTransition = transition

        setEnterSharedElementCallback(
            object : SharedElementCallback() {
                override fun onMapSharedElements(
                    names: List<String?>,
                    sharedElements: MutableMap<String?, View?>
                ) {
                    val currentFragment: Fragment = viewPager?.adapter
                        ?.instantiateItem(viewPager!!, MainActivity.currentPosition) as Fragment
                    val view: View = currentFragment.view ?: return

                    sharedElements[names[0]] = view.findViewById(R.id.movieImage)
                }
            })
    }
}