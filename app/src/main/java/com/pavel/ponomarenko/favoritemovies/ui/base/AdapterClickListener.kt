package com.pavel.ponomarenko.favoritemovies.ui.base

import android.view.View

interface AdapterClickListener<T> {
    fun onItemClick(view: View, position: Int, data: T)
}