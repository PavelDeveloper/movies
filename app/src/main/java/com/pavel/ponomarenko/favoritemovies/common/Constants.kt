package com.pavel.ponomarenko.favoritemovies.common

internal object Constants {
    const val BASE_URL  = "http://test.php-cd.attractgroup.com"
    const val POSTER_BASE_URL  = "http://s8.hostingkartinok.com/uploads/images/2016/03/"
    const val DISK_CACHE_SIZE: Long = 10 * 1024 * 1024
}