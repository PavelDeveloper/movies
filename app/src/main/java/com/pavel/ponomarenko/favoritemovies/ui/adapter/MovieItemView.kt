package com.pavel.ponomarenko.favoritemovies.ui.adapter

import android.content.Context
import android.support.design.widget.CoordinatorLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.LayoutInflater
import com.pavel.ponomarenko.favoritemovies.R
import com.pavel.ponomarenko.favoritemovies.common.ImageLoader
import com.pavel.ponomarenko.favoritemovies.data.favorite_model.Movie
import com.pavel.ponomarenko.favoritemovies.ui.base.AdapterClickListener
import com.pavel.ponomarenko.favoritemovies.ui.base.ItemModel
import kotlinx.android.synthetic.main.movie_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class MovieItemView : CoordinatorLayout, ItemModel<Movie> {

    private var listener: AdapterClickListener<Movie>? = null
    private var imageLoader: ImageLoader? = null
    private var fragment: Fragment? = null

    val v = LayoutInflater.from(context).inflate(R.layout.movie_item, this)

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, attributeSetId: Int) : super(context, attrs, attributeSetId)

    override fun setData(data: Movie) {
        imageLoader = ImageLoader()
        imageView.let {
            imageLoader?.loadImage(context, fragment, imageView, data.image)
            //imageLoader?.loadImgOld(context, fragment, imageView, data.image)
        }

        data.name.let{
            movieName.text = it
        }

        data.time.let{
            val date = Date(it.toLong())
            val format = SimpleDateFormat("dd-MMM-yyyy HH:mm", Locale.ROOT)
            movieTime?.text = format.format(date) ?: ""
        }

        imageView.setOnClickListener {
            listener?.let {
                it.onItemClick(imageView, (parent as RecyclerView).getChildAdapterPosition(this), data)
            }
        }
    }

    fun setClickListener(listener: AdapterClickListener<Movie>?) {
        this.listener = listener
    }

    fun setFragment(fragment: Fragment) {
        this.fragment = fragment
    }

}