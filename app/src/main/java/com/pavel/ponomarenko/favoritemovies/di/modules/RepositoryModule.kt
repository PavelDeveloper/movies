package com.pavel.ponomarenko.favoritemovies.di.modules

import com.pavel.ponomarenko.favoritemovies.data.repository.RepoLocalDataSource
import com.pavel.ponomarenko.favoritemovies.data.repository.RepoRemoteDataSource
import com.pavel.ponomarenko.favoritemovies.data.repository.Repository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    internal fun provideRepoLocalDataSource(): RepoLocalDataSource {
        return RepoLocalDataSource()
    }

    @Singleton
    @Provides
    internal fun provideRepoRemoteDataSource(): RepoRemoteDataSource {
        return RepoRemoteDataSource()
    }

    @Singleton
    @Provides
    internal fun provideRepository(): Repository {
        return Repository()
    }
}