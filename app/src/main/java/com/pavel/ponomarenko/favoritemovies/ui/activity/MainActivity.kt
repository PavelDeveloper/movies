package com.pavel.ponomarenko.favoritemovies.ui.activity

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.FragmentManager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.Toast
import com.pavel.ponomarenko.favoritemovies.R
import com.pavel.ponomarenko.favoritemovies.common.replaceFragment
import com.pavel.ponomarenko.favoritemovies.ui.base.BaseFragment
import com.pavel.ponomarenko.favoritemovies.ui.fragment.HomeFragment
import com.pavel.ponomarenko.favoritemovies.ui.interfaces.ToolbarActions
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), FragmentManager.OnBackStackChangedListener,
    ToolbarActions {

    private var backStackCount: Int = 0
    private lateinit var drawerToggle: ActionBarDrawerToggle
    var isDrawerFixed = false
    lateinit var drawerLayout: DrawerLayout

    companion object {
        var currentPosition = 0
        private const val KEY_CURRENT_POSITION = "currentPosition"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        isDrawerFixed = resources.getBoolean(R.bool.isDrawerFixed)

        backStackCount = savedInstanceState?.getInt("count") ?: 0
        currentPosition = savedInstanceState?.getInt(KEY_CURRENT_POSITION, 0) ?: 0

        drawerImpl()

        if (backStackCount == 0) {
            replaceFragment(HomeFragment.newInstance(), this)
        } else {
            showBackButton()
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt("count", backStackCount)
        outState?.putInt(KEY_CURRENT_POSITION, currentPosition)
    }

    override fun getToolBar(): Toolbar = toolbar as Toolbar

    override fun setSupportToolBar(toolbar: Toolbar) = setSupportActionBar(toolbar)

    override fun setToolbarTitle(toolbarTitle: String) {
        (toolbar as Toolbar).title = toolbarTitle
        (toolbar as Toolbar).subtitle = ""
    }

    override fun showBackButton() {
        setSupportActionBar(toolbar as Toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_back_arrow)
        }
    }

    override fun hideBackButton() {
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            supportActionBar!!.setDisplayShowHomeEnabled(false)
        }
    }

    override fun onBackStackChanged() {
        backStackCount = supportFragmentManager.backStackEntryCount
        val fragment = supportFragmentManager.findFragmentById(R.id.main_container)
        if (fragment is BaseFragment) {
            setToolbarTitle((fragment).getToolbarTitle())
        }
        if (backStackCount > 0) {
            showBackButton()
        }
        if (fragment is HomeFragment) {
            hideBackButton()
        }
    }

    private fun drawerImpl() {
        drawerLayout = drawer
        drawerToggle = ActionBarDrawerToggle(this, drawerLayout, R.string.Open, R.string.Close)
        drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()

        navigationMenu.setNavigationItemSelectedListener(NavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.account -> Toast.makeText(
                    this@MainActivity,
                    "My Account",
                    Toast.LENGTH_SHORT
                ).show()
                R.id.settings -> Toast.makeText(
                    this@MainActivity,
                    "Settings",
                    Toast.LENGTH_SHORT
                ).show()
                R.id.mycart -> Toast.makeText(this@MainActivity, "My Cart", Toast.LENGTH_SHORT)
                    .show()
                R.id.nav_share -> Toast.makeText(this@MainActivity, "Share", Toast.LENGTH_SHORT)
                    .show()
                R.id.nav_send -> Toast.makeText(this@MainActivity, "Send", Toast.LENGTH_SHORT)
                    .show()
                else -> return@OnNavigationItemSelectedListener true
            }
            true
        })
    }
}
