package com.pavel.ponomarenko.favoritemovies.data.detail_model

data class Genre(
    val id: Int,
    val name: String
)