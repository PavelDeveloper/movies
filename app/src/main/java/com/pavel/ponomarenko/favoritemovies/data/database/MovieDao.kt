package com.pavel.ponomarenko.favoritemovies.data.database

import android.arch.persistence.room.*
import com.pavel.ponomarenko.favoritemovies.data.favorite_model.Movie

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg model: Movie)

    @Query("SELECT * FROM movies")
    fun all(): List<Movie>

    @Query("SELECT * FROM movies WHERE itemId = :id")
    fun getMovieById(id: String): Movie

    @Query("DELETE FROM movies WHERE itemId = :id")
    fun deleteById(id: String)

    @Delete
    fun reallyDeleteMovieData(data: Movie)
}