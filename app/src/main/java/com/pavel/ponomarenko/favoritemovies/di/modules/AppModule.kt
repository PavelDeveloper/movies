package com.pavel.ponomarenko.favoritemovies.di.modules

import android.content.Context
import com.pavel.ponomarenko.favoritemovies.app.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideContext(): Context = app
}