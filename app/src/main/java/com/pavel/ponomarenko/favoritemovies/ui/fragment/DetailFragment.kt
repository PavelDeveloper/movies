package com.pavel.ponomarenko.favoritemovies.ui.fragment

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.pavel.ponomarenko.favoritemovies.R
import com.pavel.ponomarenko.favoritemovies.R.layout.fragment_detail
import com.pavel.ponomarenko.favoritemovies.common.ImageLoader
import com.pavel.ponomarenko.favoritemovies.data.favorite_model.Movie
import com.pavel.ponomarenko.favoritemovies.mvp.presenters.DetailPresenter
import com.pavel.ponomarenko.favoritemovies.mvp.view.DetailView
import com.pavel.ponomarenko.favoritemovies.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.math.abs


class DetailFragment : BaseFragment(), DetailView {

    @InjectPresenter
    lateinit var presenter: DetailPresenter

    companion object {
        const val MOVIE_KEY_ID = "movie_key_id"
        fun newInstance(movieId: String): Fragment {
            val fragment = DetailFragment()
            val argument = Bundle()
            argument.putString(MOVIE_KEY_ID, movieId)
            fragment.arguments = argument
            return fragment
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getToolbar()?.setNavigationIcon(R.drawable.ic_back_arrow)
        getToolbar()?.setNavigationOnClickListener {
            activity?.onBackPressed()
        }
        val movieId = arguments?.getString(MOVIE_KEY_ID)!!
        movieImage.transitionName = movieId
        presenter.loadInfo(id = movieId)
        changeImageSize()
    }

    private fun changeImageSize() {
        scrollView?.let {
            it.viewTreeObserver
                ?.addOnScrollChangedListener {
                    val scrollY: Int = it.scrollY
                    val offsetRate: Float =
                        scrollY.toFloat() * 0.38f / it.measuredWidth
                    val scaleFactor = 1 - abs(offsetRate)
                    if (scaleFactor > 0) {
                        movieImage?.let { img ->
                            img.scaleX = scaleFactor
                            img.scaleY = scaleFactor
                            img.translationX = -dp2px() * offsetRate
                        }
                    }
                }
        }
    }

    private fun dp2px(): Int {
        val m = context?.resources?.displayMetrics?.density ?: 0.5f
        return (80f * m + 0.5f).toInt()
    }

    override fun getLayoutRes(): Int = fragment_detail

    override fun getToolbarTitle(): String = getString(R.string.detail)

    @SuppressLint("SetTextI18n")
    override fun setMovieInfo(data: Movie) {
        description.background = ContextCompat.getDrawable(context!!, R.color.colorPrimary)
        ImageLoader().loadImage(context!!, this.parentFragment, movieImage, data.image)
        //ImageLoader().loadImgOld(context!!, this.parentFragment, movieImage, data.image)
        nameTextView.text = data.name
        descriptionTv.text = data.description
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
        description.visibility = View.GONE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
        description.visibility = View.VISIBLE
    }
}
