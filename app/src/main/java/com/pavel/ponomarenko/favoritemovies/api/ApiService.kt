package com.pavel.ponomarenko.favoritemovies.api

import com.pavel.ponomarenko.favoritemovies.data.favorite_model.Movie
import com.pavel.ponomarenko.favoritemovies.data.favorite_model.MoviesData
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("/test.json")
    fun getMoviesData(): Deferred<Response<List<Movie>>>

   /* @GET("/3/movie/{id}")
    fun getDetailMovieData(@Path("id") movieId: Int,
                           @Query("api_key") api_key: String = Constants.API_KEY): Deferred<Response<DetailMovieData>>

    @GET("/3/movie/{id}/videos")
    fun getMovieTrailers(@Path("id") movieId: Int,
                           @Query("api_key") api_key: String = Constants.API_KEY): Deferred<Response<TrailerData>>*/

}

