package com.pavel.ponomarenko.favoritemovies.ui.base

import android.support.v7.widget.RecyclerView
import android.view.View

class BaseViewHolder<out T1 : View> constructor(val view: T1) : RecyclerView.ViewHolder(view)