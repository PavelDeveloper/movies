package com.pavel.ponomarenko.favoritemovies.di.component

import com.pavel.ponomarenko.favoritemovies.data.repository.RepoLocalDataSource
import com.pavel.ponomarenko.favoritemovies.data.repository.RepoRemoteDataSource
import com.pavel.ponomarenko.favoritemovies.data.repository.Repository
import com.pavel.ponomarenko.favoritemovies.di.modules.*
import com.pavel.ponomarenko.favoritemovies.mvp.presenters.DetailPresenter
import com.pavel.ponomarenko.favoritemovies.mvp.presenters.HomePresenter
import com.pavel.ponomarenko.favoritemovies.ui.adapter.MoviePagerAdapter
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [AppModule::class, ApiModule::class, NetManagerModule::class,
        DatabaseModule::class, RepositoryModule::class]
)
@Singleton
interface AppComponent {

    fun inject(detailPresenter: DetailPresenter)
    fun inject(repoRemoteDataSource: RepoRemoteDataSource)
    fun inject(repository: Repository)
    fun inject(repoLocalDataSource: RepoLocalDataSource)
    fun inject(homePresenter: HomePresenter)
    fun inject(moviePagerAdapter: MoviePagerAdapter)
}