package com.pavel.ponomarenko.favoritemovies.mvp.view

import com.arellomobile.mvp.MvpView

interface BaseMvpView : MvpView {

    fun showProgress()

    fun hideProgress()
}