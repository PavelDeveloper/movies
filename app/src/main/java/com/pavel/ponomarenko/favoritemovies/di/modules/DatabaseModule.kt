package com.pavel.ponomarenko.favoritemovies.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import com.pavel.ponomarenko.favoritemovies.data.database.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    @Inject
    internal fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context,
            AppDatabase::class.java, "database")
            .allowMainThreadQueries()
            .build()
    }
}