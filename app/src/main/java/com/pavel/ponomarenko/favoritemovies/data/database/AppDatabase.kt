package com.pavel.ponomarenko.favoritemovies.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.pavel.ponomarenko.favoritemovies.data.detail_model.DetailMovieData
import com.pavel.ponomarenko.favoritemovies.data.favorite_model.Movie

@Database(entities = [Movie::class, DetailMovieData::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}