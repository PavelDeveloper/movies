package com.pavel.ponomarenko.favoritemovies.data.favorite_model

data class MoviesData(
    val page: Int,
    val total_results: Int,
    val total_pages: Int,
    val results: List<Movie>?
)