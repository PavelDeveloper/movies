package com.pavel.ponomarenko.favoritemovies.ui.fragment


import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.SharedElementCallback
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.RecyclerView
import android.transition.TransitionInflater
import android.transition.TransitionSet
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.pavel.ponomarenko.favoritemovies.R
import com.pavel.ponomarenko.favoritemovies.common.replaceFragment
import com.pavel.ponomarenko.favoritemovies.data.favorite_model.Movie
import com.pavel.ponomarenko.favoritemovies.mvp.presenters.HomePresenter
import com.pavel.ponomarenko.favoritemovies.mvp.view.HomeView
import com.pavel.ponomarenko.favoritemovies.ui.activity.MainActivity
import com.pavel.ponomarenko.favoritemovies.ui.adapter.MovieAdapter
import com.pavel.ponomarenko.favoritemovies.ui.base.AdapterClickListener
import com.pavel.ponomarenko.favoritemovies.ui.base.BaseFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*


class HomeFragment : BaseFragment(), HomeView, AdapterClickListener<Movie> {

    @InjectPresenter
    lateinit var presenter: HomePresenter

    private lateinit var movieAdapter: MovieAdapter
    private val moviesIdList: ArrayList<String> = ArrayList()

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        prepareTransitions()
        postponeEnterTransition()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (!resources.getBoolean(R.bool.isDrawerFixed)) {
            getToolbar()?.setNavigationIcon(R.drawable.ic_menu_24dp)
            getToolbar()?.setNavigationOnClickListener {
                activity?.drawer?.openDrawer(GravityCompat.START)
            }
        } else {
            getToolbar()?.navigationIcon = null
        }
        movieAdapter = MovieAdapter(this)
        movieAdapter.setClickListener(this)
        view.recyclerView.apply {
            adapter = movieAdapter
        }
        scrollToPosition()
    }

    override fun getLayoutRes(): Int = R.layout.fragment_home

    override fun getToolbarTitle(): String = getString(R.string.app_name)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onItemClick(view: View, position: Int, data: Movie) {
        MainActivity.currentPosition = position
        this.startPostponedEnterTransition()
        (this.exitTransition as TransitionSet?)!!.excludeTarget(view, true)
        replaceFragment(PagerFragment.newInstance(moviesIdList), view)
    }

    override fun setMovies(data: List<Movie>) {
        if (data.isEmpty()) {
            Toast.makeText(context, getString(R.string.error_connection), Toast.LENGTH_LONG).show()
        } else {
            movieAdapter.addData(data)
        }
    }

    override fun getMoviesIdList(data: List<String>) {
        moviesIdList.clear()
        moviesIdList.addAll(data)
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        baseHomeLayout.background = ContextCompat.getDrawable(context!!, R.color.colorPrimaryDark)
        progressBar.visibility = View.GONE
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun prepareTransitions() {
        exitTransition = TransitionInflater.from(context)
            .inflateTransition(R.transition.grid_exit_transition)

        setExitSharedElementCallback(
            object : SharedElementCallback() {
                override fun onMapSharedElements(
                    names: List<String?>,
                    sharedElements: MutableMap<String?, View?>
                ) {
                    val selectedViewHolder = recyclerView
                        .findViewHolderForAdapterPosition(MainActivity.currentPosition)
                        ?: return

                    sharedElements[names[0]] =
                        selectedViewHolder.itemView.findViewById(R.id.imageView)
                }
            })
    }

    private fun scrollToPosition() {
        recyclerView.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View,
                left: Int,
                top: Int,
                right: Int,
                bottom: Int,
                oldLeft: Int,
                oldTop: Int,
                oldRight: Int,
                oldBottom: Int
            ) {
                recyclerView.removeOnLayoutChangeListener(this)
                val layoutManager: RecyclerView.LayoutManager? = recyclerView.layoutManager
                val viewAtPosition: View? =
                    layoutManager?.findViewByPosition(MainActivity.currentPosition)
                if (viewAtPosition == null || layoutManager
                        .isViewPartiallyVisible(viewAtPosition, false, true)
                ) {
                    recyclerView.post { layoutManager?.scrollToPosition(MainActivity.currentPosition) }
                }
            }
        })
    }
}
